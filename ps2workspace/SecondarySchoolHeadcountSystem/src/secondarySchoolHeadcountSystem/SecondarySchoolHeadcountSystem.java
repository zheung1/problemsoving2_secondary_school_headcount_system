package secondarySchoolHeadcountSystem;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class SecondarySchoolHeadcountSystem {
	private static DecimalFormat df = new DecimalFormat("#.##");
	public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		
		
		System.out.println("Welcome to Secondary School Headcount System");
		System.out.println("(Simplified CLI version for SCSJ4383 PS2 to show the concept of DBC)");
		System.out.println("=======================================================");
		
		System.out.println("Please select an option");
		System.out.println("1. Calculate lower secondary student grade average (gred purata pelajar)");
		System.out.println("2. Calculate student OTI (Operational Targeted Increment)");
		
		System.out.print("Please enter a number to select option => ");
		
		int option = scanner.nextInt();
		
		switch (option){
			case 1 : 
				System.out.print("Please enter number of taken subjects => ");
				int subjectCount = scanner.nextInt();
				GradeAverage g = new GradeAverage();
				double gredPurata = g.calcGradeAverage(subjectCount);
				System.out.println("The grade average (gred purata) of the student is " + gredPurata);
				break;
			
			case 2 : 
				System.out.print("Please enter the mark of previous final exam (TOV) => ");
				double tov = scanner.nextDouble();
				
				System.out.print("Please enter the expected target final exam mark (ETR) => ");
				double etr = scanner.nextDouble();
				
				Target target = new Target(tov, etr);
				double oti1 = target.calcOti1();
				double oti2 = target.calcOti2(oti1);
				
				System.out.println("==============");
				System.out.println("Student Targets");
				System.out.println("==============");
				System.out.println("TOV (Previous Final Exam Marks): " + tov + "%");
				System.out.println("OTI1 (Test 1 Target): " + df.format(oti1)+ "%");
				System.out.println("OTI2: (Mid Term Target):" + df.format(oti2)+ "%");
				System.out.println("ETR (Upcoming Final Exam Target): " + etr+ "%");
				
				break;
			default: break;
		}
	}
	
	
	
}
