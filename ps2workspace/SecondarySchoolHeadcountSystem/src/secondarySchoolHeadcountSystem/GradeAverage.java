package secondarySchoolHeadcountSystem;

import java.util.ArrayList;
import java.util.Scanner;


public class GradeAverage {
	
	
	public double calcGradeAverage(int subjectCount){
		//precondition
		assert subjectCount > 0: "Subject count must be more than zero";
		
		//operation
		Scanner scanner = new Scanner(System.in);
		ArrayList subjectNames = new ArrayList();
		ArrayList subjectMarks = new ArrayList();
		double gredPurata = 0;
		int[] gradeCounts = {0,0,0,0,0,0};
		
		for(int i=0; i<subjectCount; i++){
			
			String subjectName = "";
			double subjectMark = 0;
			System.out.print("Please enter subject name => ");
			subjectName = scanner.nextLine();
			System.out.print("Please enter subject mark => ");
			
			try{
				subjectMark = scanner.nextDouble();
				scanner.nextLine(); 
			}catch(Exception e){
				subjectMark = 0;
			}
			subjectNames.add(subjectName);
			subjectMarks.add(subjectMark);
			int gradeIndex = getGrade(subjectMark);
			gradeCounts[gradeIndex]++;
		}
		
		double sum = 0;
		
		for(int i=0; i<gradeCounts.length; i++){
			sum += gradeCounts[i] * (i+1);
		}
		
		gredPurata = sum/subjectCount;
		
		//postcondition: gredPurata > 0
		assert gredPurata > 0: "The calculated grade average should be greater than 0";
		
		return gredPurata;
	}
	
	public int getGrade(double subjectMark){
		
		//precondition
		assert subjectMark >= 0 && subjectMark <= 100: "subject mark must be a value between 0 and 100 inclusive";
		
		//operation
		int gradeIndex;
		
		if(subjectMark >= 85 && subjectMark <= 100){
			gradeIndex = 0;
		}else if(subjectMark >= 70 && subjectMark <= 84){
			gradeIndex = 1;
		}else if(subjectMark >= 60 && subjectMark <= 69){
			gradeIndex = 2;
		}else if(subjectMark >= 50 && subjectMark <= 59){
			gradeIndex = 3;
		}else if(subjectMark >= 40 && subjectMark <= 49){
			gradeIndex = 4;
		}else{
			gradeIndex = 5;
		}
		
		//postcondition gradeIndex >= 0 && gradeIndex <= 5
		assert gradeIndex >= 0 && gradeIndex <= 5: "gradeIndex should between 0 and 5";
		
		return gradeIndex;
	}
}















