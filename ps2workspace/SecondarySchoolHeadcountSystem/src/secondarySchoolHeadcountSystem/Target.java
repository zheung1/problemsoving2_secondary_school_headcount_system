package secondarySchoolHeadcountSystem;

public class Target {
	
	private double tov, etr;
	
	Target(double tov, double etr){
		
		//precondition
		assert etr >= tov: "ETR must be greater than or equal to TOV";
		assert tov >= 0 && tov <= 100: "TOV must be a value between 0 and 100 inclusive";
		assert etr >= 0 && etr <= 100: "ETR must be a value between 0 and 100 inclusive";
		this.tov = tov;
		this.etr = etr;
	}
	
	//operation
	public double calcOti1(){
		
		double oti1;
		
		oti1 = tov + (etr - tov)/3;
		
		//postcondition oti1 >= tov
		assert oti1>=tov: "the calculated oti1 should be equal or greater than tov";
		assert etr >= tov: "the value of etr must still invariantly be equal or greater than tov";
		
		return oti1;	
	}
	
	public double calcOti2(double oti1){
		
		//precondition
		assert oti1 >= tov: "OTI1 must be greater than or equal to TOV";
		assert etr >= tov: "the value of etr must be equal or greater than tov";
		
		double oti2;
		
		//operation
		oti2 = oti1 + (etr - tov)/3;
		
		//postcondition oti2>= oti1
		assert oti2>= oti1: "the calculated oti2 should be equal or greater than oti1";
		assert etr >= tov: "the value of etr must still invariantly be equal or greater than tov";
		
		return oti2;
	}
	
}
